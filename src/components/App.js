import React from 'react';
import './App.css';
import MovieComponent from './MovieComponent';

const App = () => {
  return (
      <MovieComponent />
  );
}

export default App;