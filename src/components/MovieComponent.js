import React, { Component } from 'react';
import './components-styles/search-component-style.css';

class MovieComponent extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
       movieList: [],
       indexedMovies: [],
       allMovies: [],
       movieName: '',
       movieRating: 0.0,
       movieLength: '',
    }
    this.registerMovie = this.registerMovie.bind(this);
 };

 onChangeMovieName = (e) => {
  this.setState({
    movieName: e.target.value
  });
 }

 onChangeMovieRating = (e) => {
  this.setState({
    movieRating: e.target.value
  });
 }

 onChangeMovieLength = (e) => {
  this.setState({
    movieLength: e.target.value
  });
 }

 registerMovie = () => {

    var movieObject = {
      "name": this.state.movieName,
      "rating": this.state.movieRating,
      "length": this.state.movieLength
    };

    var regexp = /^[0-9]+([,.][0-9]+)?$/g;
    var checkRating = regexp.test(movieObject.rating);

    if(movieObject.name.trim() === "" && movieObject.rating === 0.0 && movieObject.length.trim() === "") {
      alert("All fields are mandatory");
      return false;
    }

    if(movieObject.name.trim() !== "" && checkRating === false) {
      alert("The rating only accepts numbers");
      return false;
    }

    if(movieObject.length !== "" && !movieObject.length.trim().endsWith("h") && !movieObject.length.trim().endsWith("m")) {
      alert("The length's format must end with h(hours) or m(minutes)");
      return false;
    }

    if(this.searchForMovie() === false) {
      return false;
    } else {
      this.setState({
        movieList: this.state.movieList.concat(movieObject),
        allMovies: this.state.movieList.concat(movieObject)
      });
    }

  } 

  searchForMovie = () => {

    var newestMovieName = this.state.movieName.toUpperCase();
      var movieNames = this.state.movieList;
      for(var i = 0; i < movieNames.length; i++) {
        if(movieNames[i].name.toString().toUpperCase() === newestMovieName) {
          alert("There's already a movie with this name.");
          return false;
      }
    }

  }

  // Method to look for a specific movie.
  indexMovies = (e) => {

   var movieName = e.target.value.toString().toUpperCase();
   var foundMovies = [];

   if(movieName.trim() !== "") {

    for(var i = 0; i < this.state.allMovies.length; i++) {
      if(this.state.allMovies[i].name.toString().toUpperCase().startsWith(movieName)) {
       foundMovies.push(this.state.allMovies[i]);
      }
    }
 
     var movieResults = foundMovies.filter((foundMovies, index, self) =>
     index === self.findIndex((t) => (t.name === foundMovies.name)));
 
     this.setState({
       movieList: movieResults
     });

   } else {
    this.setState({
      indexedMovies: [],
      movieList: this.state.allMovies
    });
   }

  }

 render() {
  return (

    <div>

        <div id="register-movie-form">

          <form method="POST" onSubmit={this.registerMovie}>
            <h4>Movie's name</h4>
            <input type="text" id = "name-input" onChange={this.onChangeMovieName.bind(this)} required />
      
            <h4>Rating</h4>
            <input type="text" id = "ratings-input" onChange={this.onChangeMovieRating.bind(this)} required />
      
            <h4>Movie's length</h4>
            <input type="text" id = "duration-input" onChange={this.onChangeMovieLength.bind(this)} required />
            <br />
            <button type="submit" id = "submit-button" onClickCapture={e => { e.preventDefault(); }} onClick={this.registerMovie}>Submit</button>
          </form>

          </div>

          <div id="search-movie-form">

            <form method="POST" onSubmitCapture={e => { e.preventDefault(); }} onSubmit={this.indexMovies}>
      
              <div id="search-movie-form">
  
                <h4>Search</h4>
                <input type="text" id = "search-input" onChange={this.indexMovies} />

                <table border="1" id="movies-table">
                  <thead>
                    <tr>
                        <th>Name</th>
                        <th>Ratings</th>
                        <th>Duration</th>
                    </tr>
                  </thead>
                  <tbody>
                  {this.state.movieList.map((movie, index) =>
                    <tr key={index}>
                      <td>{movie.name}</td>
                      <td>{movie.rating}</td>
                      <td>{movie.length}</td>
                    </tr>
                    )}
                    <tr>
                      <td colSpan="3" id="no-result">No movies found.</td>
                    </tr>
                </tbody>
              </table>

            </div>
            </form>

          </div>

    </div>

    );
  }
}
export default MovieComponent;